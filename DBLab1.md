## Lab1
# Task №1
Была реализована БД заметок, где для каждого user'а существует список созданных им листов тэгов и заметок. Для каждой заметки из списка заметок существует список тэгов из пользовательского списка тэгов. Интерфейс БД представлен на рисунке ниже. Она была приведена к 3 НФ.

![image](uploads/8686207ef75fbf446132dbe9d6fe632f/image.png)

# Task №2
Представим полученную БД с SQL ddl.

```
CREATE TABLE IF NOT EXISTS public."user"
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    username character varying(32) NOT NULL,
    CONSTRAINT user_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.note
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    share_data timestamp with time zone NOT NULL,
    header character varying NOT NULL,
    complex_text text NOT NULL,
    link varchar(2083),
    "time" timestamp with time zone,
	CONSTRAINT note_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.note_list
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    user_id integer NOT NULL,
    name character varying NOT NULL,
	CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES "user"(id) ON DELETE CASCADE,
	CONSTRAINT note_list_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.note_list_item
(
	note_id bigint,
  	note_list_id bigint,
  	PRIMARY KEY (note_id, note_list_id),
  	CONSTRAINT fk_note FOREIGN KEY(note_id) REFERENCES note(id),
  	CONSTRAINT fk_note_list FOREIGN KEY(note_list_id) REFERENCES note_list(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS public.tag
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    name character varying NOT NULL,
    mark character varying NOT NULL,
    priority integer NOT NULL,
    CONSTRAINT tag_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.tag_list
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    user_id integer NOT NULL,
    name character varying(32) NOT NULL,
	CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES "user"(id) ON DELETE CASCADE,
    CONSTRAINT tag_list_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.tag_list_item
(
	tag_id bigint,
  	tag_list_id bigint,
  	PRIMARY KEY (tag_id, tag_list_id),
  	CONSTRAINT fk_tag FOREIGN KEY(tag_id) REFERENCES tag(id),
  	CONSTRAINT fk_tag_list FOREIGN KEY(tag_list_id) REFERENCES tag_list(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS public.note_tag
(
	note_id bigint,
  	tag_id bigint,
  	PRIMARY KEY (note_id, tag_id),
  	CONSTRAINT fk_note FOREIGN KEY(note_id) REFERENCES note(id),
  	CONSTRAINT fk_tag FOREIGN KEY(tag_id) REFERENCES tag(id) ON UPDATE CASCADE ON DELETE CASCADE
);
```

# Task №3
Запросы для заполнения BD примером данных.
```insert into "user" (username)
values ('user1');

insert into note (share_data, header, complex_text, link, time)
values ('1900-01-01 00:00:00.000 -05:00', 'test1', 'complexTextTest', 'linkTest', '1900-01-01 00:00:00.000 -05:00');

insert into note_list (user_id, name)
values (1, 'testNoteListForUser1');

insert into note_list_item (note_id, note_list_id)
OVERRIDING SYSTEM VALUE
select
    u.id,
    l.id
from note_db.public."user" u
inner join note_db.public.note_list l
   on l.id = u.id;

insert into tag (name, mark, priority)
values ('testName', 'testMark', 1);

insert into tag_list (user_id, name)
values (1, 'testTagListForUser1');

insert into tag_list_item (tag_id, tag_list_id)
OVERRIDING SYSTEM VALUE
select
    u.id,
    l.id
from note_db.public."user" u
inner join note_db.public.tag_list l
   on l.id = u.id;

insert into tag_note (tag_id, note_id)
OVERRIDING SYSTEM VALUE
select
    t.id,
    n.id
from note_db.public.tag t
inner join note_db.public.note n
   on t.id = n.id;
```
