import datetime
import psycopg2
import random
import argparse
import configparser
from django.db import transaction

config = configparser.ConfigParser()
config.read('config.ini', encoding='utf-8')
#######################################################################################################################################################################
connection = psycopg2.connect(
    dbname=config.get("postgres", "dbname"),
    user=config.get("postgres", "user"),
    password=config.get("postgres", "password"))

cursor = connection.cursor()

digits = '0123456789'
letters = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
en_letters = 'abcdefghijklmnopqrstuvwxyz'

today = datetime.datetime.today()

tamer = ()
halls = ()


# destination = ()
# manufacturers = ()
# carriers = ()
# country_of_origin = ()
# fees = ()
# transportation = ()
# declarations = ()
# product = ()
# types = ()


def gen_data(arguments):
    if arguments.truncate is not None and int(arguments.truncate) == 1:
        truncate_db()

    else:
        if arguments.n is not None:
            get_note(arguments.n)

        if arguments.u is not None:
            get_user(arguments.u)

        if arguments.t is not None:
            get_tag(arguments.t)

        if arguments.nl is not None:
            get_note_list(arguments.nl)

        if arguments.tl is not None:
            get_tag_list(arguments.tl)

        if arguments.nli is not None:
            get_note_list_item(arguments.nli)

        if arguments.tli is not None:
            get_tag_list_item(arguments.tli)

        if arguments.nt is not None:
            get_note_tag(arguments.nt)


def truncate_db():
    cursor.execute("truncate table \"user\" restart identity cascade;"
                   "truncate table tag_list restart identity cascade;"
                   "truncate table tag restart identity cascade;"
                   "truncate table note restart identity cascade;"
                   "truncate table note_list restart identity cascade;"
                   "truncate table tag_list_item restart identity cascade;"
                   "truncate table note_tag restart identity cascade;"
                   "truncate table note_list_item restart identity cascade;"
                   )


def get_note(amount):
    global note
    share_data = []
    header = []
    complex_text = []
    link = []
    time = []
    temp = []

    for i in range(int(amount)):
        share_data.append(str(rnd_date(2015, 2020)) + " " + str(rnd_time()))
        header.append(rnd_string(random.randint(5, 15)))
        complex_text.append(rnd_string(random.randint(5, 15)))
        link.append(rnd_string(random.randint(5, 15)))
        time.append(str(rnd_date(2015, 2020)) + " " + str(rnd_time()))
        temp.append(tuple((share_data[i], header[i], complex_text[i], link[i], time[i],)))
    note_rows = tuple(temp)
    query = "INSERT INTO note (share_data, header, complex_text, link, time) VALUES (%s, %s, %s, %s, %s)"
    cursor.executemany(query, note_rows)
    cursor.execute('SELECT id FROM note ')
    note = cursor.fetchall()


def get_user(amount):
    global user
    username = []
    temp = []
    for i in range(int(amount)):
        username.append(rnd_string(random.randint(5, 15)))
        temp.append(tuple((username[i],)))
    user_rows = tuple(temp)
    query = "INSERT INTO \"user\" (username) VALUES (%s)"
    cursor.executemany(query, user_rows)
    cursor.execute('SELECT id FROM \"user\" ')
    user = cursor.fetchall()


def get_tag(amount):
    global tag
    name = []
    mark = []
    priority = []
    temp = []
    for i in range(int(amount)):
        name.append(rnd_string(random.randint(5, 15)))
        mark.append(rnd_string(random.randint(5, 15)))
        priority.append(random.randint(10, 20))
        temp.append(tuple((name[i], mark[i], priority[i],)))
    tag_rows = tuple(temp)
    query = "INSERT INTO tag (name, mark, priority) VALUES (%s, %s, %s)"
    cursor.executemany(query, tag_rows)
    cursor.execute('SELECT id FROM tag')
    tag = cursor.fetchall()


def get_note_list(amount):
    global note_list

    cursor.execute('SELECT id FROM \"user\"')
    user_id = cursor.fetchall()
    m_user = []
    name = []

    temp = []
    for i in range(int(amount)):
        name.append(rnd_string(random.randint(10, 20)))
        m_user.append(random.choice(user_id)[0])
        temp.append(tuple((m_user[i], name[i],)))
    note_list_rows = tuple(temp)
    query = "INSERT INTO note_list (user_id, name) VALUES (%s, %s)"
    cursor.executemany(query, note_list_rows)
    cursor.execute('SELECT id FROM note_list ')
    note_list = cursor.fetchall()


def get_tag_list(amount):
    global tag_list

    cursor.execute('SELECT id FROM \"user\"')
    user_id = cursor.fetchall()
    m_user = []
    name = []

    temp = []
    for i in range(int(amount)):
        name.append(rnd_string(random.randint(10, 20)))
        m_user.append(random.choice(user_id)[0])
        temp.append(tuple((m_user[i], name[i],)))
    tag_list_rows = tuple(temp)
    query = "INSERT INTO tag_list (user_id, name) VALUES (%s, %s)"
    cursor.executemany(query, tag_list_rows)
    cursor.execute('SELECT id FROM tag_list')
    tag_list = cursor.fetchall()


def get_note_list_item(amount):
    a = 0
    cursor.execute('SELECT id FROM note')
    note_id = cursor.fetchall()

    cursor.execute('SELECT id FROM note_list')
    note_list_id = cursor.fetchall()

    m_note = []
    m_note_list = []

    temp = []
    for i in range(int(amount)):
        #m_note.append(random.choice(note_id)[0])
        m_note.append(note_id[a][0])
        m_note_list.append(note_list_id[a][0])
        temp.append(tuple((m_note[i], m_note_list[i],)))
        a = a + 1
    note_list_item_rows = tuple(temp)
    query = "INSERT INTO note_list_item (note_id, note_list_id) VALUES (%s, %s)"
    cursor.executemany(query, note_list_item_rows)



def get_tag_list_item(amount):
    a = 0
    cursor.execute('SELECT id FROM tag')
    tag_id = cursor.fetchall()

    cursor.execute('SELECT id FROM tag_list')
    tag_list_id = cursor.fetchall()

    m_tag = []
    m_tag_list = []

    temp = []
    for i in range(int(amount)):
        #m_tag.append(random.choice(tag_id)[0])
        m_tag.append(tag_id[a][0])
        m_tag_list.append(random.choice(tag_list_id)[0])
        temp.append(tuple((m_tag[i], m_tag_list[i],)))
        a = a + 1
    tag_list_item_rows = tuple(temp)
    query = "INSERT INTO tag_list_item (tag_id, tag_list_id) VALUES (%s, %s)"
    cursor.executemany(query, tag_list_item_rows)


def get_note_tag(amount):
    a = 0
    cursor.execute('SELECT id FROM note')
    note_id = cursor.fetchall()

    cursor.execute('SELECT id FROM tag')
    tag_id = cursor.fetchall()

    cursor.execute('SELECT note_id FROM note_tag')
    note_tag_id = cursor.fetchall()

    cursor.execute('SELECT tag_id FROM note_tag')
    note_tag_id = cursor.fetchall()

    m_note = []
    m_tag = []

    temp = []

    for i in range(int(amount)):
        #m_note.append(random.choice(note_id)[0])
        m_note.append(note_id[a][0])
        m_tag.append(random.choice(tag_id)[0])
        temp.append(tuple((m_note[i], m_tag[i],)))
        a += 1
    note_tag_rows = tuple(temp)
    query = "INSERT INTO note_tag (note_id, tag_id) VALUES (%s, %s)"
    cursor.executemany(query, note_tag_rows)




def rnd_bool():
    num = random.randint(0, 100)
    if num < 50:
        return True
    else:
        return False


def rnd_time():
    hours = random.randint(1, 23)
    minutes = random.randint(0, 59)
    seconds = random.randint(0, 59)
    time = datetime.time(hours, minutes, seconds)
    return time


def rnd_date(start_year, end_year):
    year = random.randint(start_year, end_year)
    month = random.randint(1, 12)
    day = random.randint(1, 28)
    date = datetime.date(year, month, day)
    return date


def rnd_phone_number():
    phone_number = ""
    for i in range(8):
        phone_number += random.choice(digits)
    return phone_number


def rnd_string(length):
    str = ''
    for i in range(int(length)):
        str += random.choice(letters)
    return str


if __name__ == '__main__':
    args = argparse.ArgumentParser(description="Details of data generation")
    args.add_argument('--n', action="store", dest="n")
    args.add_argument('--u', action="store", dest="u")
    args.add_argument('--t', action="store", dest="t")
    args.add_argument('--nl', action="store", dest="nl")
    args.add_argument('--tl', action="store", dest="tl")
    args.add_argument('--nli', action="store", dest="nli")
    args.add_argument('--tli', action="store", dest="tli")
    args.add_argument('--nt', action="store", dest="nt")

    args.add_argument('--truncate', action="store", dest="truncate")

    arguments = args.parse_args()
    gen_data(arguments)
    connection.commit()
    connection.close()
