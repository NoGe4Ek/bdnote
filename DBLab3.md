## Lab3

### Список стандартных запросов

1. Сделайте выборку всех данных из таблицы.

```sql
SELECT * from "user";
```

Результат:

![](https://gitlab.com/NoGe4Ek/bdnote/-/raw/main/img/1.png)

2. Сделайте выборку данных из одной таблицы при нескольких условиях, с использованием логических операций, `LIKE`, `BETWEEN`, `IN` (не менее 3-х разных примеров).

```sql
SELECT * from note where header like '%теф%';
```

Выбираем все заметки, в названии которых есть "теф". Результат:

![](https://gitlab.com/NoGe4Ek/bdnote/-/raw/main/img/2_1.png)

```sql
SELECT * from note where share_data between '2016-01-01' AND '2017-01-01';
```

Выбираем все заметки, отправленные с 2010 по 2014. Результат:

![](https://gitlab.com/NoGe4Ek/bdnote/-/raw/main/img/2_2.png)

```sql
SELECT * from tag where mark in ('ебвхръъхьё', 'опщмюагт');
```

Выбираем все тэги с представленными пометками. Результат:

![](https://gitlab.com/NoGe4Ek/bdnote/-/raw/main/img/2_3.png)

3. Создайте в запросе вычисляемое поле.

```sql
SELECT * from tag where (17 - 2 - priority) = 3;
```

Симуляция необходиости поиска приоритета по заданному ограничению. Результат:

![](https://gitlab.com/NoGe4Ek/bdnote/-/raw/main/img/3.png)

4. Сделайте выборку всех данных с сортировкой по нескольким полям.

```sql
SELECT * from note where CAST(time AS DATE) = '2015-04-17' ORDER BY header, link;
```

Выбираем все заметки, у которых стоит дата напоминания на 17 апреля и сортируем их по заголовку и, к примеру, по ссылке. Результат:

![](https://gitlab.com/NoGe4Ek/bdnote/-/raw/main/img/4.png)

5. Создайте запрос, вычисляющий несколько совокупных характеристик таблиц.

```sql
SELECT AVG(priority), MAX(priority), MIN(priorityS) from tag;
```

Выбираем среднее, максимальное и минимальное значение приоритета тэгов. Результат:

![](https://gitlab.com/NoGe4Ek/bdnote/-/raw/main/img/5.png)

6. Сделайте выборку данных из связанных таблиц (не менее двух примеров).

```sql
SELECT * FROM note_list INNER JOIN "user" u ON note_list.user_id = u.id;
```

Выбираем все списки заметок вместе с их владельцами. Результат:

![](https://gitlab.com/NoGe4Ek/bdnote/-/raw/main/img/6_1.png)

```sql
SELECT * FROM tag_list INNER JOIN "user" u ON tag_list.id = u.id;
```

Выбираем все списки тэгов вместе с их владельцами. Результат:

![](https://gitlab.com/NoGe4Ek/bdnote/-/raw/main/img/6_2.png)

7. Создайте запрос, рассчитывающий совокупную характеристику с использованием группировки, наложите ограничение на результат группировки.

```sql
SELECT header, COUNT(*) FROM note GROUP BY header HAVING header != 'тыжиошюсъзк';
```

Считаем количество включений хэдеров в заметки, за исключением определенного заголовка. Результат:

![](https://gitlab.com/NoGe4Ek/bdnote/-/raw/main/img/7_1.png)

7. Придумайте и реализуйте пример использования вложенного запроса.

```sql
SELECT * FROM note_list WHERE user_id = (SELECT id from "user" WHERE username = 'гёюэф');
```

Получаем все листы заметок, имя владельца которых = гёюэф. Результат:

![](https://gitlab.com/NoGe4Ek/bdnote/-/raw/main/img/7_2.png)

8. С помощью оператора UPDATE измените значения нескольких полей у всех записей, отвечающих заданному условию.

```sql
UPDATE tag SET priority = '1' WHERE mark < 'ръгипт';
```

8. С помощью оператора DELETE удалите запись, имеющую максимальное (минимальное) значение некоторой совокупной характеристики.

```sql
DELETE from note INNER JOIN "note_tag" nt ON note.id = nt.note_id;
```

Удалим все заметки, у которых нет тэгов.

9. С помощью оператора DELETE удалите записи в главной таблице, на которые не ссылается подчиненная таблица.

```sql
DELETE from user WHERE id NOT IN (SELECT user_id FROM note_list);
```

Удалим всех людей, у которых нет списков заметок.


#### Индивидуальное задание:


#### Выводы:

В данной лабораторной работе я опробовал SQL-DML, выполнил ряд заданий из лабораторной работы и индивидуальное задание, которое показалось мне достаточно интересным, особенно для изучения и пробы SQL-DML.


